class Pagination {
	constructor (entity, params = {}) {
		this.entity = entity
		this.limit = +params.limit || 10
		this.query = params.query || {}
		this.select = params.select
		this.page = +params.page || 1
		this.sort = params.sort || '-_id'
	}

	async getReturn () {

		const count = await this.entity.countDocuments(this.query)

		const offset = (this.page - 1) * this.limit
		const totalPages = Math.ceil((count / this.limit))
		const realPages = (count / this.limit)

		if (totalPages && this.page > totalPages ) {
			return {
				contents: [],
				totalCount: 0,
				page: this.page,
				totalPages: this.page,
				nextPage: null,
				previousPage: null,
				limit: this.limit
			}
		}
		
		const data = await this.entity.find(this.query)
			.select(this.select)
			.sort(this.sort)
			.limit(this.limit)
			.skip(offset)
			.lean()
		
		let nextPage = null
		let previousPage = null

		if (this.page === 1) {
			if (realPages > 1) {
				nextPage = `?page=${this.page + 1}&limit=${this.limit}`

				if (this.select) {
					nextPage += `&select=${this.select}` 
				}
				
			}
		} else if (this.page === totalPages) {
			nextPage = null
			previousPage = `?page=${this.page - 1}&limit=${this.limit}`

			if (this.select) {
				previousPage += `&select=${this.select}` 
			}

		} else {
			nextPage = `?page=${this.page + 1}&limit=${this.limit}`
			previousPage = `?page=${this.page - 1}&limit=${this.limit}`

			if (this.select) {
				nextPage += `&select=${this.select}`
				previousPage += `&select=${this.select}` 
			}
		}

		return {
			contents: data,
			totalCount: count,
			page: this.page,
			totalPages,
			nextPage,
			previousPage,
			limit: this.limit
		}
	}
}

module.exports.Pagination = Pagination