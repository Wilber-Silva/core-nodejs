const Responses = require('./response')
const Exceptions = require('./exceptions')

module.exports = {
	...Responses,
	...Exceptions
}