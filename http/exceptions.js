class NotFoundException extends Error {
	constructor(message) {
		super(message)
		this.name = 'NotFoundException'
	}
}

class UnprocessableEntityException extends Error {
	constructor(message) {
		super(message)
		this.name = 'UnprocessableEntityException'
	}
}

class BadRequestException extends Error {
	constructor({ message, errors }) {
		super(message)
		this.name = 'BadRequestException'
		this.errors = errors
	}
}

class UnauthorizedException extends Error {
	constructor(message) {
		super(message)
		this.name = 'UnauthorizedException'
	}
}

module.exports = {
	NotFoundException,
	UnprocessableEntityException,
	BadRequestException,
	UnauthorizedException
}