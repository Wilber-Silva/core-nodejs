const {
	SUCCESS, 
	CREATED_SUCCESS,
	IM_USED,
	NOT_CONTENT,
	NOT_FOUND,
	BAD_REQUEST,
	UNPROCESSABLE_ENTITY,
	UNAUTHORIZED,
	INTERNAL_ERROR
} = require('./status-code')

const {
	NotFoundException,
	UnprocessableEntityException,
	BadRequestException,
	UnauthorizedException
} = require('./exceptions')

const defaultSuccessResponse = {
	message: "No message"
}

class Response {
	constructor(statusCode, body = defaultSuccessResponse) {
		this.statusCode = statusCode
		this.body = JSON.stringify(body)
	}
}

class ResponseSuccess {
	constructor(body = defaultSuccessResponse) {
		this.statusCode = SUCCESS
		this.body = JSON.stringify(body)
	}
}

class ResponseCreated {
	constructor(body = defaultSuccessResponse) {
		this.statusCode = CREATED_SUCCESS
		this.body = JSON.stringify(body)
	}
}

class ResponseNotContent {
	constructor() {
		this.statusCode = NOT_CONTENT
		this.body = null
	}
}

class ResponseImUsed {
	constructor(message) {
		this.statusCode = IM_USED
		this.body = JSON.stringify({ message })
	}
}

class ResponseError {
	constructor(e) {
		if ( e instanceof NotFoundException ) {
			this.statusCode = NOT_FOUND
			this.body = this.mountBody(e.message) 
		} else if (e instanceof UnprocessableEntityException ) {
			this.statusCode = UNPROCESSABLE_ENTITY
			this.body = this.mountBody(e.message) 
		} else if (e instanceof BadRequestException) {
			this.statusCode = BAD_REQUEST
			this.body = this.mountBody(e.message, e.errors) 
		} else if (e instanceof UnauthorizedException) {
			this.statusCode = UNAUTHORIZED
			this.body = this.mountBody(e.message) 
		} else {
			this.statusCode = INTERNAL_ERROR
			this.body = this.mountBody(e.message) 
		}
	}

	mountBody = (message, errors) => JSON.stringify({ message, errors })
}

module.exports = {
	Response,
	ResponseOK: ResponseSuccess,
	ResponseImUsed,
	ResponseSuccess,
	ResponseCreated,
	ResponseNotContent,
	ResponseError
}