const { BadRequestException } = require('./../http')

module.exports.bearerToken = async (bearer) => {
	// if (!headers.Authorization) throw new BadRequestException('Token not provided')

	// const authSplit = headers.Authorization.split(' ')

	const authSplit = bearer.split(' ')

	if (authSplit.length !== 2) throw new BadRequestException('Token Error')

	const [ scheme, token ] = authSplit

	if (! /^Bearer$/i.test(scheme)) throw new BadRequestException('Token malformated')

	return token
}