class Parse {
	toRouteName = async(string) => string.toLowerCase()
																	.replace(' ', '-')
																	.replace(new RegExp('[ÁÀÂÃáàâã]','gi'), 'a')
																	.replace(new RegExp('[ÉÈÊéèê]','gi'), 'e')
																	.replace(new RegExp('[ÍÌÎíìî]','gi'), 'i')
																	.replace(new RegExp('[ÓÒÔÕóòôõ]','gi'), 'o')
																	.replace(new RegExp('[ÚÙÛúùû]','gi'), 'u')
																	.replace(new RegExp('[Ççó]','gi'), 'c')
}

module.exports = new Parse()